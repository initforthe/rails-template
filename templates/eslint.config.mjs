import prettier from 'eslint-plugin-prettier'
import globals from 'globals'
import babelParser from '@babel/eslint-parser'
import path from 'node:path'
import { fileURLToPath } from 'node:url'
import js from '@eslint/js'
import { FlatCompat } from '@eslint/eslintrc'
const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

const compat = new FlatCompat({
  baseDirectory: __dirname,
  recommendedConfig: js.configs.recommended,
  allConfig: js.configs.all,
})

export default [
  {
    ignores: ['app/assets/build/*.js', 'vendor/**/*.js'],
  },
  ...compat.extends('prettier'),
  {
    plugins: {
      prettier,
    },

    languageOptions: {
      globals: {
        ...globals.browser,
      },

      parser: babelParser,
      ecmaVersion: 12,
      sourceType: 'module',

      parserOptions: {
        allowImportExportEverywhere: false,

        ecmaFeatures: {
          globalReturn: false,
        },
      },
    },

    rules: {
      'space-before-function-paren': 0,
      'new-cap': 0,
      'prettier/prettier': 2,
    },
  },
]
