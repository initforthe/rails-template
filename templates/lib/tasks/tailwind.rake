# frozen_string_literal: true

require 'tailwindcss/commands_ext'

Rake::Task['tailwindcss:build'].clear
Rake::Task['tailwindcss:watch'].clear

namespace :tailwindcss do
  desc 'Build your Tailwind CSS'
  task :build do |_, args|
    debug = args.extras.include?('debug')

    files = Rails.root.join('app/assets/stylesheets').glob('*.tailwind.css')

    if files.count == 1 && files.first.basename == 'application.tailwind.css'
      command = Tailwindcss::Commands.compile_command(debug:)
      puts command.join(' ')
      system(*command, exception: true)
    else
      files.map do |file|
        Tailwindcss::Commands.compile_file_command(file:, debug:)
      end.each do |command| # rubocop:disable Style/MultilineBlockChain
        puts command.join(' ')
        system(*command, exception: true)
      end
    end
  end

  desc 'Watch and build your Tailwind CSS on file changes'
  task :watch do |_, args|
    debug = args.extras.include?('debug')
    poll = args.extras.include?('poll')

    files = Rails.root.join('app/assets/stylesheets').glob('*.tailwind.css')

    if files.count == 1 && files.first.basename == 'application.tailwind.css'
      command = Tailwindcss::Commands.watch_command(debug:, poll:)
      puts command.join(' ')
      system(*command)
    else
      trap('SIGINT') { exit }

      files.map do |file|
        Tailwindcss::Commands.watch_file_command(file:, debug:, poll:)
      end.each do |command| # rubocop:disable Style/MultilineBlockChain, Lint/ShadowingOuterLocalVariable
        fork do
          puts command.join(' ')
          system(*command)
        end
      end
    end
  end
end

Rake::Task['assets:precompile'].enhance(['tailwindcss:build'])

if Rake::Task.task_defined?('spec:prepare')
  # https://github.com/rspec/rspec-rails/issues/2610
  Rake::Task['spec:prepare'].enhance(['tailwindcss:build'])
elsif Rake::Task.task_defined?('test:prepare')
  Rake::Task['test:prepare'].enhance(['tailwindcss:build'])
elsif Rake::Task.task_defined?('db:test:prepare')
  Rake::Task['db:test:prepare'].enhance(['tailwindcss:build'])
end
