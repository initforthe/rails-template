# frozen_string_literal: true

# Docker Helpers
module DockerHelpers
  def upload_fixture_file(src, content_type, binary = false) # rubocop:disable Style/OptionalBooleanParameter
    path = Rails.root.join(src)
    original_filename = ::File.basename(path)

    tempfile = Tempfile.open(original_filename) # create a new tempfile

    content = File.read(path) # Extract content of the original file
    tempfile.write content # Write all content from original file to the tempfile

    Rack::Test::UploadedFile.new(tempfile, content_type, binary, original_filename: original_filename)
  end
end
