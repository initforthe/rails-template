# frozen_string_literal: true

module Tailwindcss
  # TailwindCSS Extension to allow multiple files to be compiled at once using the @config directive
  module CommandsExt
    def compile_file_command(file:, debug: false, **) # rubocop:disable Metrics/MethodLength, Metrics/AbcSize
      file = Pathname.new(file.to_s)
      input = file
      output = Rails.root.join('app/assets/builds', file.basename.sub('tailwind.', ''))
      config = input.open(&:readline).start_with?('@config') ? nil : Rails.root.join('config/tailwind.config.js')
      [
        executable(**),
        '-i', input.to_s,
        '-o', output.to_s
      ].tap do |command|
        command << '-c' if config
        command << config.to_s if config
        command << '--minify' unless debug
      end
    end

    def watch_file_command(poll: false, **)
      compile_file_command(**).tap do |command|
        command << '-w'
        command << '-p' if poll
      end
    end
  end
end

Tailwindcss::Commands.extend(Tailwindcss::CommandsExt)
