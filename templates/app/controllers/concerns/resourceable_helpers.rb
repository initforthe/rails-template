# frozen_string_literal: true

# Resourceable
#
# Provides a number of resource helpers for controllers supporting Crudable
module ResourceableHelpers
  extend ActiveSupport::Concern

  included do
    helper_method :resource
  end

  def resource
    instance_variable_get("@#{resource_var_name}")
  end
end
