# frozen_string_literal: true

# Dashboards controller
class DashboardsController < BaseController
  skip_after_action :verify_policy_scoped
  skip_after_action :verify_authorized

  def show; end
end
