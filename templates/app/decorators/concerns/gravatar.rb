# frozen_string_literal: true

require 'digest/md5'

# Gravatar Concern
module Gravatar
  extend ActiveSupport::Concern

  def avatar(options = {})
    size = options.delete(:size) || 75
    options.reverse_merge!(alt: email)
    h.image_tag gravatar_url(size), options
  end

  private

  def gravatar_url(size)
    "https://www.gravatar.com/avatar/#{gravatar_hash}?d=#{CGI.escape(fallback_url(size))}&s=#{size}"
  end

  def fallback_url(_size)
    "https://eu.ui-avatars.com/api/#{CGI.escape(name)}/128/FBE6EE/D30050"
  end

  def gravatar_hash
    Digest::MD5.hexdigest email.strip.downcase
  end
end
