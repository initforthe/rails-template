# frozen_string_literal: true

# app/decorators/paginating_decorator.rb
class PaginatingDecorator < Draper::CollectionDecorator
  delegate :current_page, :total_entries, :total_pages, :per_page, :offset, :limit_value
end
