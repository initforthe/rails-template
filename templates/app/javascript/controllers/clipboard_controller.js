import { Controller } from '@hotwired/stimulus'

export default class extends Controller {
  static targets = [ "source" ]

  copy(event) {
    event.preventDefault()
    const copiedValues = this.sourceTargets.map((element) => {
      return element.innerHTML
    }).join("\n")
    navigator.clipboard.writeText(copiedValues)

    const copyEvent = new Event('clipboard:copied', {
      cancelable: false,
      bubbles: true,
    })
    this.element.dispatchEvent(copyEvent)
  }
}
