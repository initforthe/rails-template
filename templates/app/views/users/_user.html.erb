<li id="<%%= dom_id(user) %>" class="col-span-1 bg-white divide-y divide-gray-200 rounded-lg shadow">
  <div class="flex items-center justify-between w-full p-6 space-x-6">
    <div class="flex-1 truncate">
      <div class="flex items-center space-x-3">
        <h3 class="text-sm font-medium text-gray-900 truncate"><%%= user.name %></h3>
        <%% if user.deleted? %>
          <span class="inline-flex flex-shrink-0 items-center rounded-full bg-red-50 px-1.5 py-0.5 text-xs font-medium text-red-700 ring-1 ring-inset ring-red-600/20">deleted</span>
        <%% end %>
      </div>
      <p class="mt-1 text-sm text-gray-500 truncate"><%%= user.email %></p>
    </div>
    <%%= user.decorate.avatar(class: 'h-10 w-10 flex-shrink-0 rounded-full bg-gray-300') %>
  </div>
  <div>
    <div class="flex -mt-px divide-x divide-gray-200">
      <div class="flex flex-1 w-0">
        <a href="mailto:<%%= user.email %>" class="relative inline-flex items-center justify-center flex-1 w-0 py-4 -mr-px text-sm font-semibold text-gray-400 border border-transparent rounded-bl-lg hover:text-gray-600 gap-x-3">
          <svg class="w-5 h-5" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
            <path d="M3 4a2 2 0 00-2 2v1.161l8.441 4.221a1.25 1.25 0 001.118 0L19 7.162V6a2 2 0 00-2-2H3z" />
            <path d="M19 8.839l-7.77 3.885a2.75 2.75 0 01-2.46 0L1 8.839V14a2 2 0 002 2h14a2 2 0 002-2V8.839z" />
          </svg>
          Email
        </a>
      </div>
      <%% if policy(user).edit? %>
        <div class="flex flex-1 w-0">
          <%%= link_to [:edit, user], class: 'relative inline-flex items-center justify-center flex-1 w-0 py-4 -mr-px text-sm font-semibold text-gray-400 hover:text-blue-400 border border-transparent rounded-bl-lg gap-x-3', data: { turbo_frame: 'modal' } do %>
            <%%= heroicon 'pencil', variant: :solid, options: { class: 'w-5 h-5 mr-2' } %>
            Edit
          <%% end %>
        </div>
      <%% end %>
      <%% if policy(user).destroy? %>
        <div class="flex flex-1 w-0">
          <%%= button_to [user], method: :delete, class: 'inline-flex', form_class: 'relative inline-flex items-center justify-center flex-1 w-0 py-4 text-sm font-semibold text-gray-400 hover:text-red-800 border border-transparent rounded-br-lg gap-x-3', form: { data: { turbo_frame: :_top, turbo_confirm: 'Are you sure you want to delete this user? They will no longer be able to sign in' } } do %>
            <%%= heroicon 'trash', variant: :solid, options: { class: 'w-5 h-5 mr-2' } %>
            Delete
          <%% end %>
        </div>
      <%% end %>
      <%% if policy(user).restore? %>
        <div class="flex flex-1 w-0">
          <%%= button_to [user, :reactivations], method: :post, class: 'inline-flex', form_class: 'relative inline-flex items-center justify-center flex-1 w-0 py-4 text-sm font-semibold text-gray-400 hover:text-green-800 border border-transparent rounded-br-lg gap-x-3', form: { data: { turbo_frame: :_top, turbo_confirm: 'Are you sure you want to restore this user? Their account will be reactivated and able to sign in again' } } do %>
            <%%= heroicon 'check', variant: :solid, options: { class: 'w-5 h-5 mr-2' } %>
            Restore
          <%% end %>
        </div>
      <%% end %>
    </div>
  </div>
</li>
