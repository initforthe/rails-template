# frozen_string_literal: true

require 'net/http/post/multipart'
require 'mime/types'

# Browserless Helpers
module BrowserlessHelpers
  # rubocop:disable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/MethodLength, Metrics/PerceivedComplexity
  def attach_remote_file(locator = nil, paths, make_visible: nil, **options) # rubocop:disable Style/OptionalArguments
    raise ArgumentError, '`#attach_file` does not support passing both a locator and a block' if locator && block_given?

    remote_paths = []

    Array(paths).each do |path|
      raise Capybara::FileNotFound, "cannot attach file, #{path} does not exist" unless File.exist?(path.to_s)

      # Upload local files to browserless.io instance
      remote_paths << browserless_file(path)
    end
    remote_paths = remote_paths.first if remote_paths.one?

    options[:allow_self] = true if locator.nil?

    if block_given?
      begin
        execute_script Capybara::Node::Actions::CAPTURE_FILE_ELEMENT_SCRIPT
        yield
        file_field = evaluate_script 'window._capybara_clicked_file_input'
        raise ArgumentError, "Capybara was unable to determine the file input you're attaching to" unless file_field
      rescue ::Capybara::NotSupportedByDriverError
        warn 'Block mode of `#attach_remote_file` is not supported by the current driver - ignoring.'
      end
    end
    # Allow user to update the CSS style of the file input since they are so often hidden on a page
    if make_visible
      ff = file_field || find(:file_field, locator, **options.merge(visible: :all))
      while_visible(ff, make_visible) { |el| el.set(remote_paths) }
    else
      (file_field || find(:file_field, locator, **options)).set(remote_paths)
    end
  end
  # rubocop:enable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/MethodLength, Metrics/PerceivedComplexity

  private

  def browserless_file(path)
    request = Net::HTTP::Post::Multipart.new('/workspace',
                                             file: UploadIO.new(path.to_s,
                                                                mime_type(path)))
    uri = URI.parse(ENV['CHROME_URL'])
    response = Net::HTTP.start(uri.host, uri.port) { |http| http.request(request) }
    json = JSON.parse(response.body, symbolize_names: true).first
    json[:path]
  end

  def mime_type(path)
    MIME::Types.type_for(path.to_s).first.content_type
  end
end

RSpec.configure do |config|
  config.include BrowserlessHelpers, type: :system
end
