# frozen_string_literal: true

require 'docker_helpers'

RSpec.configure do |config|
  config.include DockerHelpers if ENV['CI'] || ENV['REMOTE_CONTAINERS']
end
