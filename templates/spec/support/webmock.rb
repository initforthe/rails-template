# frozen_string_literal: true

require 'webmock/rspec'

allowed = []

allowed += [CURRENT_HOSTNAME, /chrome/] if ENV['CI'] || ENV['REMOTE_CONTAINERS']

WebMock.disable_net_connect! allow_localhost: true, allow: allowed
