# frozen_string_literal: true

postmark_environments = %w[production]
postmark_environments << 'review' if @review_env
postmark_environments << 'staging' if @staging_env

postmark_environments.each do |environment|

  inject_into_file "config/environments/#{environment}.rb",
                   "\n  mailer_host = ENV.fetch('MAILER_HOST', nil)\n",
                   after: 'Rails.application.configure do'

  inject_into_file "config/environments/#{environment}.rb", after: /config.action_mailer.perform_caching.*\n/ do <<-'RUBY'
  config.action_mailer.default_url_options = { host: mailer_host, only_path: false }
  config.action_mailer.asset_host = mailer_host
  config.action_mailer.delivery_method = :postmark
  config.action_mailer.postmark_settings = {
    api_token: ENV.fetch('POSTMARK_SERVER_TOKEN') do
      Rails.application.credentials.postmark[Rails.env.to_sym]
    end
  }
  RUBY
  end

end
