# frozen_string_literal: true

generate 'config:install'

# Add a mail sender placeholder we can reference in the app
inject_into_file 'config/settings.yml', "mail:\n  sender: no-reply@example.com\n"
