remote_template 'gitlab-ci.yml.erb', '.gitlab-ci.yml'
remote_template 'docker-compose.yml.erb', 'docker-compose.yml'
remote_template '.docker-compose/review.yml', '.docker-compose/review.yml'
remote_template '.docker-compose/production.yml', '.docker-compose/production.yml'

append_to_file '.dockerignore' do
  <<-IGNORES
  db/*.sqlite3
  db/*.sqlite3-journal
  Dockerfile
  README.rdoc
  vendor/ruby
  coverage
  *.sql
  public/packs-test
  bin/postmarkctl
  IGNORES
end

remote_template 'Dockerfile.erb', 'Dockerfile'

create_file '.gitlab/README.md' do
  <<-README
  # GitLab CI Jobs
  #
  # Put your custom Gitlab CI jobs here.
  # Make sure they are named with a .yml extension.
  #
  README
end

run 'bundle lock --add-platform x86_64-linux-musl'
