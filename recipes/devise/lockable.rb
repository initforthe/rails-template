# Set Devise - Lockable
gsub_file @model_file, 'devise :database_authenticatable', 'devise :database_authenticatable, :lockable'
uncomment_lines @devise_migration, ':failed_attempts'
uncomment_lines @devise_migration, ':unlock_token'
uncomment_lines @devise_migration, ':locked_at'
