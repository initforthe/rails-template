#  Add devise masquerade
remote_template "app/controllers/masquerades_controller.rb.erb", "app/controllers/masquerades_controller.rb"
inject_into_file @model_file, ", :masqueradable", after: ", :validatable"
