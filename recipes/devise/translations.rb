# Add translations
append_to_file 'config/locales/titles.en.yml' do <<-YML
  devise:
    invitations:
      title: Complete your sign up
    sessions:
      title: Sign in
    #{@user_plural}:
      title: #{@authentication_model.underscore.capitalize}
      new:
        title: Invite a new #{@authentication_model.underscore.capitalize}
      create:
        title: Invite a new #{@authentication_model.underscore.capitalize}
  YML
end
