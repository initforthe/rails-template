# Include the user decorator and the Gravatar concern
remote_template 'app/decorators/concerns/gravatar.rb', 'app/decorators/concerns/gravatar.rb'
remote_template 'app/decorators/user_decorator.rb.erb', "app/decorators/#{@user_singular}_decorator.rb"
