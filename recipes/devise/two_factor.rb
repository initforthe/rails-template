generate "devise_two_factor #{@authentication_model}"
generate "migration add_devise_two_factor_backupable_to_#{@user_plural}"
twofa_migration = Dir.glob("db/migrate/*add_devise_two_factor_backupable_to_#{@user_plural}.rb").first
inject_into_file twofa_migration,
"    add_column :#{@user_plural}, :otp_backup_codes, :string, array: true\n",
after: "def change\n"

remote_template "app/controllers/sessions_controller.rb.erb", "app/controllers/sessions_controller.rb"
remote_template "app/controllers/two_factor_settings_controller.rb.erb", "app/controllers/two_factor_settings_controller.rb"
remote_template "app/controllers/concerns/authenticate_with_otp_two_factor.rb.erb", "app/controllers/concerns/authenticate_with_otp_two_factor.rb"
remote_template "app/helpers/qr_code_helper.rb", "app/helpers/qr_code_helper.rb"
remote_template "app/javascript/controllers/clipboard_controller.js", "app/javascript/controllers/clipboard_controller.js"
gsub_file @model_file, "  devise :two_factor_authenticatable\n", ''
inject_into_file @model_file,
                  ",\n         :two_factor_authenticatable, :two_factor_backupable,\n         sign_in_after_reset_password: false,\n         otp_backup_code_length: 10,\n          otp_number_of_backup_codes: 10",
                  after: ", :validatable"
