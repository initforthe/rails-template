# Add `name` to users
inject_into_file @devise_migration, "\n      t.string :name", after: /\#\# Database authenticatable$/
inject_into_file @model_file,
                 "\n   validates :name, presence: true\n",
                 before: 'end'
