# Set Devise parent mailer to use our Applciation Mailer for layout and future customisation
gsub_file 'config/initializers/devise.rb', /^.*config\.parent_mailer.*/, '  config.parent_mailer = \'ApplicationMailer\''

# Set some basic Devise security options
gsub_file 'config/initializers/devise.rb', /^.*config\.lock_strategy.*/, '  config.lock_strategy = :failed_attempts'
gsub_file 'config/initializers/devise.rb', /^.*config\.maximum_attempts.*/, '  config.maximum_attempts = 5'
gsub_file 'config/initializers/devise.rb', /^.*config\.unlock_strategy.*/, '  config.unlock_strategy = :both'
gsub_file 'config/initializers/devise.rb', /^.*config\.unlock_in.*/, '  config.unlock_in = 1.hour'
gsub_file 'config/initializers/devise.rb', /^.*config\.last_attempt_warning.*/, '  config.last_attempt_warning = true'
gsub_file 'config/initializers/devise.rb', /^.*config\.reset_password_within.*/, '  config.reset_password_within = 6.hours'
