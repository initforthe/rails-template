# Factory
inject_into_file "spec/factories/#{@user_singular.pluralize}.rb",
                 "name { FFaker::Name.name }\nemail { FFaker::Internet.email }\npassword { 'password' }\n",
                 after: "factory :#{@user_singular} do\n"
