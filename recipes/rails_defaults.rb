# frozen_string_literal: true

application do
  <<~CONFIG
    config.generators do |g|
      g.test_framework :rspec, fixture: false
      g.stylesheets false
      g.javascripts false
      g.helper false
    end
  CONFIG
end

append_to_file '.gitignore', <<~GITIGNORE
  # Ignore database.yml
  config/database.yml
GITIGNORE

gsub_file 'app/views/layouts/application.html.erb', '<html>', '<html lang="<%= I18n.locale %>">'
inject_into_file 'app/views/layouts/application.html.erb', before: '</head>' do
  <<~HTML
    \n    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
  HTML
end

generate 'annotate:install'

if @configured_options.database == 'postgresql'
  remote_template 'config/database.yml.example.erb', 'config/database.yml.example'
  run 'cp config/database.yml.example config/database.yml'
else
  run 'cp config/database.yml config/database.yml.example'
end
