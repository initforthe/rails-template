# App Controller
inject_into_file 'app/controllers/application_controller.rb', after: /ActionController\:\:Base\n/ do <<-'RUBY'
  add_flash_types(:success, :confirm, :alert, :warning, :error, :danger, :info)

  layout 'simple'
  RUBY
end
remote_template 'app/controllers/base_controller.rb.erb', 'app/controllers/base_controller.rb'
