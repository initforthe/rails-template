# Jumpstart Devise features
recipe 'jumpstart/devise/authentication'
recipe 'jumpstart/devise/soft_delete'
recipe 'jumpstart/devise/views'
recipe 'jumpstart/devise/reactivations'
recipe 'jumpstart/devise/profile_management'
recipe 'jumpstart/devise/policies'
if @devise_roles&.any?
  recipe 'jumpstart/devise/roles'
else
  gsub_file "app/policies/#{@user_singular}_policy.rb", 'admin?', 'true'
end
recipe 'jumpstart/devise/scopes'
recipe 'jumpstart/devise/policies'
recipe 'jumpstart/devise/current'
recipe 'jumpstart/devise/controllers'
recipe 'jumpstart/devise/navigation'
recipe 'jumpstart/devise/invitable' if @devise_invitable
recipe 'jumpstart/devise/two_factor' if @devise_2fa
recipe 'jumpstart/devise/cleanup'
recipe 'jumpstart/devise/masquerade' if @devise_masquerade
