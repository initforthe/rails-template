# Dashboard
remote_template 'app/controllers/dashboards_controller.rb', 'app/controllers/dashboards_controller.rb'
remote_template 'app/views/dashboards/show.html.erb', 'app/views/dashboards/show.html.erb'

# Routes for Dashboard
inject_into_file 'config/routes.rb', "\n\n  root to: 'dashboards#show'", after: '# Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html'
inject_into_file 'config/routes.rb', "\n\n  resource :dashboards, only: :show", after: "root to: 'dashboards#show'"

# Title local for DashBoard
inject_into_file 'config/locales/titles.en.yml', after: 'en:' do
  "\n  dashboards:\n    show:\n      title: Dashboard"
end
