# Decorators
remote_template 'app/decorators/paginating_decorator.rb', 'app/decorators/paginating_decorator.rb'

inject_into_file 'app/decorators/application_decorator.rb', after: /Draper\:\:Decorator\n/ do <<-'RUBY'
  delegate_all

  def self.collection_decorator_class
    PaginatingDecorator
  end
RUBY
end
