unless @authentication_model == 'User'
  [[/users/, @authentication_model.underscore.pluralize], [/user/, @authentication_model.underscore],  [/Users/, @authentication_model.pluralize], [/User/, @authentication_model]].each do |regex, replacement|
    gsub_file "app/views/#{@user_plural}/new.html.erb", regex, replacement
    gsub_file "app/views/#{@user_plural}/_form.html.erb", regex, replacement
    gsub_file "app/views/#{@user_plural}/edit.html.erb", regex, replacement
    gsub_file "app/views/#{@user_plural}/index.html.erb", regex, replacement
    gsub_file "app/views/#{@user_plural}/_#{@user_singular}.html.erb", regex, replacement
    gsub_file("app/views/#{@user_plural}/_scopes.html.erb", regex, replacement)
    gsub_file 'app/views/profile/accounts/_form.html.erb', regex, replacement
    gsub_file 'app/views/profile/accounts/edit.html.erb',regex, replacement
    gsub_file 'app/views/profile/accounts/update.turbo_stream.erb', regex, replacement
    if @devise_invitable
      gsub_file "app/views/#{@user_plural}/invitations/new.html.erb", regex, replacement
      gsub_file "app/views/#{@user_plural}/invitations/_form.html.erb", regex, replacement
    end
  end
end
