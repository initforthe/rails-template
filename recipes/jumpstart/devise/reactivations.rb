# Add reactivations for devise model
remote_template 'app/controllers/users/reactivations_controller.rb.erb', "app/controllers/#{@user_plural}/reactivations_controller.rb"
append_to_file 'config/locales/en.yml' do
  <<-YAML
  #{@user_plural}:
    reactivations:
      create:
        success: #{@user_singular.capitalize} has been successfully reactivated
  YAML
end
inject_into_file 'config/routes.rb',
                 " do\n     resources :reactivations, only: :create, controller: '#{@user_plural}/reactivations'\n   end\n",
                 after: "resources :#{@user_plural}"
