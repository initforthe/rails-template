# Add basic CRUD for devise model
remote_template 'app/views/application/create.turbo_stream.erb', 'app/views/application/create.turbo_stream.erb'
remote_template 'app/views/application/update.turbo_stream.erb', 'app/views/application/update.turbo_stream.erb'
inject_into_file 'config/routes.rb',"\n  resources :#{@user_plural}\n", after: '# Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html'
remote_template 'app/controllers/users_controller.rb.erb', "app/controllers/#{@user_plural}_controller.rb"
remote_template 'app/controllers/concerns/resourceable_helpers.rb'
remote_template 'app/views/users/new.html.erb', "app/views/#{@user_plural}/new.html.erb"
remote_template 'app/views/users/_form.html.erb', "app/views/#{@user_plural}/_form.html.erb"
remote_template 'app/views/users/edit.html.erb', "app/views/#{@user_plural}/edit.html.erb"
remote_template 'app/views/users/index.html.erb', "app/views/#{@user_plural}/index.html.erb"
remote_template 'app/views/users/_user.html.erb', "app/views/#{@user_plural}/_#{@user_singular}.html.erb"
remote_template('app/views/users/_scopes.html.erb', "app/views/#{@user_plural}/_scopes.html.erb") if @devise_roles&.any?
remote_template 'app/views/application/_pagination.html.erb', 'app/views/application/_pagination.html.erb'
inject_into_file 'config/locales/titles.en.yml', after: 'en:' do
  "\n  #{@user_plural}:\n    show:\n      title: #{@user_singular.titleize}\n    index:\n      title: #{@user_plural.titleize}"
end
