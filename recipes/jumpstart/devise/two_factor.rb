form_text_2fa =
<<-ERB

    <% if policy(user).edit_two_factor_authentication? %>
      <div class="grid mt-6 space-y-4">
        <% if user.otp_required_for_login %>
          <p class="text-sm">Two factor authentication is enabled.</p>
          <%= link_to two_factor_settings_path, data: { turbo_confirm: 'Are you sure you want to disable two factor authentication?', turbo_method: :delete }, class: "rounded-full inline-flex bg-white px-4 py-2.5 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50" do %>
            <%= heroicon 'x-mark', variant: :solid, options: { class: "-ml-0.5 mr-2 h-5 w-5 text-red-600" }%>
            Disable Two Factor Authentication
          <% end %>
        <% else %>
          <p class="text-sm">Two factor authentication is NOT enabled.</p>
          <%= link_to new_two_factor_settings_path, data: { turbo_frame: '_top' },  class: "rounded-full inline-flex bg-white px-4 py-2.5 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50" do %>
            <%= heroicon 'check', variant: :solid, options: { class: "-ml-0.5 mr-2 h-5 w-5 text-green-600" } %>
            Enable Two Factor Authentication
          <% end %>
        <% end %>
      </div>
    <% end %>

ERB
inject_into_file "app/views/#{@user_plural}/_form.html.erb", form_text_2fa, before: '    <div class="mt-5 sm:mt-6">'
inject_into_file "app/views/profile/accounts/_form.html.erb", form_text_2fa, before: '    <div class="mt-5 sm:mt-6">'
remote_template 'app/views/devise/sessions/_two_factor.html.erb', 'app/views/devise/sessions/_two_factor.html.erb'
remote_template 'app/views/two_factor_settings/edit.html.erb', 'app/views/two_factor_settings/edit.html.erb'
remote_template 'app/views/two_factor_settings/new.html.erb', 'app/views/two_factor_settings/new.html.erb'
gsub_file 'app/views/two_factor_settings/new.html.erb', 'current_user', "current_#{@user_singular}"
inject_into_file 'config/routes.rb', ", controllers: { sessions: 'sessions' }", after: "path: 'auth'"
inject_into_file 'config/routes.rb', "  resource :two_factor_settings, except: %i[index show]\n", before: "\n  # Reveal health status"
append_to_file 'config/locales/titles.en.yml' do
<<-YML
  sessions:
    new:
      title: Sign in
  two_factor_settings:
    new:
      title: Two Factor Authentication Setup
    edit:
      title: Two Factor Authentication Setup
YML
end
