# Basic auth specs (relies on nav bar and other jumpstart features)
remote_template 'spec/system/authentication_spec.rb.erb', 'spec/system/authentication_spec.rb'
gsub_file 'config/routes.rb', /devise_for :#{@user_plural}/, "devise_for :#{@user_plural}, path: 'auth'"
