#  Roles
system "rails g migration add_roles_to_#{@user_plural} role:integer"
inject_into_file @model_file,
                 "\n\n  enum role: { #{@devise_roles.map.with_index { |role, i| "#{role}: #{i}"}.join(', ')} }, _default: '#{@devise_roles&.first}'\n",
                 after: "acts_as_paranoid"
inject_into_file "spec/factories/#{@user_plural}.rb",
                 @devise_roles.map { |role| "\n  trait :#{role} do\n    role { '#{role}' }\n  end\n"}.join("\n"),
                 before: "  end\nend"

scope_role_text =
<<-ERB
      <%- #{@authentication_model}.roles.each do |role| %>
        <%= active_link_to #{@user_plural}_path(scope: role[0]), active: { scope: role[0] }, class: 'group relative min-w-0 flex-1 overflow-hidden bg-white py-4 px-4 text-center text-sm font-medium hover:bg-gray-50 focus:z-10', class_active: 'text-gray-900', class_inactive: 'text-gray-500 hover:text-gray-700' do %>
          <span><%= t(role[0], scope: 'roles') %></span>
          <span class="inline-flex items-center px-2 py-1 ml-2 text-xs font-medium text-gray-600 rounded-md bg-gray-50 ring-1 ring-inset ring-gray-500/10"><%= User.send(role[0]).count %></span>
          <% if is_active_link?(#{@user_plural}_path(scope: role[0]), { scope: role[0] }) %>
            <span aria-hidden="true" class="bg-indigo-600 absolute inset-x-0 bottom-0 h-0.5"></span>
          <% end %>
        <% end %>
      <% end %>
ERB
inject_into_file "app/views/#{@user_plural}/_scopes.html.erb",
                  scope_role_text,
                  before: "      <%= active_link_to users_path(scope: :only_deleted)"

scope_text =
<<-ERB
    <%- #{@authentication_model}.roles.each do |role| %>
      <option value="<%= #{@user_plural}_path(scope: role[0]) %>" <%= 'selected' if is_active_link?(users_path(scope: role[0]), { scope: role[0] }) %>><%= t(role[0], scope: 'roles') %></option>
    <% end %>
ERB
inject_into_file "app/views/#{@user_plural}/_scopes.html.erb", scope_text, after: 'All</option>'

remote_template 'app/javascript/controllers/select_navigation_controller.js', 'app/javascript/controllers/select_navigation_controller.js'
gsub_file "app/policies/#{@user_singular}_policy.rb", 'admin?', "#{@devise_roles&.first}?"
@role_form_text =
<<-ERB

      <% if policy(#{@user_singular}).edit_role? %>
        <%= vc_input_group f: f, name: :role do |c| %>
          <%= vc_select f: c.f, name: c.name, options: #{@authentication_model}.roles.keys %>
        <% end %>
      <% end %>
ERB
inject_into_file "app/views/#{@user_plural}/_form.html.erb",
                  @role_form_text,
                  after: '      <%= render(Initforthe::FormErrorsComponent.new(f.object)) %>'
inject_into_file "app/views/#{@user_plural}/_#{@user_singular}.html.erb",
                  "<span class=\"inline-flex flex-shrink-0 items-center rounded-full bg-green-50 px-1.5 py-0.5 text-xs font-medium text-green-700 ring-1 ring-inset ring-green-600/20\"><%= #{@user_singular}.role %></span>\n",
                  before: "        <% if user.deleted? %>"
