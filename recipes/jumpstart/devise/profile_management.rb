# Profile management
remote_template 'app/controllers/profile/accounts_controller.rb.erb', 'app/controllers/profile/accounts_controller.rb'
remote_template 'app/views/profile/accounts/_form.html.erb', 'app/views/profile/accounts/_form.html.erb'
remote_template 'app/views/profile/accounts/edit.html.erb', 'app/views/profile/accounts/edit.html.erb'
remote_template 'app/views/profile/accounts/update.turbo_stream.erb', 'app/views/profile/accounts/update.turbo_stream.erb'
inject_into_file 'config/routes.rb', after: '# Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html' do
  "\n\n  namespace :profile do\n    resource :account, only: %i[edit update]\n  end"
end
remote_template 'spec/system/users_spec.rb.erb', "spec/system/#{@user_plural}_spec.rb"
