user_partial_text =
  <<-ERB
      <% if policy(#{@user_singular}).impersonate? %>
        <div class="flex flex-1 w-0">
          <%= link_to masquerade_path(#{@user_singular}), data: { turbo: false }, class: 'relative inline-flex items-center justify-center flex-1 w-0 py-4 text-sm font-semibold text-gray-400 hover:text-blue-800 border border-transparent rounded-br-lg gap-x-3' do %>
            <%= heroicon 'users', variant: :solid, options: { class: 'w-5 h-5' } %>
            View as
          <% end %>
        </div>
      <% end %>
  ERB
inject_into_file "app/views/#{@user_plural}/_#{@user_singular}.html.erb", user_partial_text, before: "    </div>\n  </div>\n</li>"
remote_template 'app/views/users/_masquerade.html.erb', "app/views/#{@user_plural}/_masquerade.html.erb"
gsub_file "app/views/#{@user_plural}/_masquerade.html.erb", 'current_user', "current_#{@user_singular}"
app_text =
<<-ERB

      <% if #{@user_singular}_masquerade? %>
        <%= render '#{@user_plural}/masquerade' %>
      <% end %>

  ERB
inject_into_file 'app/views/layouts/application.html.erb', app_text, after: "      <%= render 'navbar' %>"
if @devise_2fa
  inject_into_file 'config/routes.rb', ", masquerades: 'masquerades'", after: "sessions: 'sessions'"
else
  inject_into_file 'config/routes.rb', ", controllers: { masquerades: 'masquerades' }", after: "path: 'auth'"
end
