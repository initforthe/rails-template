# Add soft delete for devise model
system "rails g migration add_deleted_at_to_#{@user_plural} deleted_at:datetime:index"
remote_template 'app/models/concerns/user_authentication.rb.erb', "app/models/concerns/#{@user_singular}_authentication.rb"
gsub_file "app/models/concerns/#{@user_singular}_authentication.rb", /User/, @authentication_model
inject_into_file @model_file,
                "\n  include #{@authentication_model}Authentication\n\n  acts_as_paranoid",
                after: '< ApplicationRecord'
inject_into_file "spec/factories/#{@user_plural}.rb",
                "  trait :deleted do\n    after :create do |record|\n      record.destroy\n    end\n  end\n",
                before: "  end\nend"
