# Add Inviteable
append_to_file 'config/locales/en.yml' do
  "    invitations:\n      create:\n        success: New user invite has been sent\n"
end

# Invitations routes
inject_into_file 'config/routes.rb',
                  "\n  namespace :#{@user_plural} do\n    resources :invitations, only: %i[new create]\n  end",
                  after: "only: :create, controller: '#{@user_plural}/reactivations'\n   end\n"

remote_template 'app/views/devise/invitations/_form.html.erb', 'app/views/devise/invitations/_form.html.erb'
remote_template 'app/views/devise/invitations/edit.html.erb', 'app/views/devise/invitations/edit.html.erb'
remote_template 'app/views/devise/invitations/new.html.erb', 'app/views/devise/invitations/new.html.erb'
remote_template 'app/views/devise/invitations/new.turbo_stream.erb', 'app/views/devise/invitations/new.turbo_stream.erb'
remote_template 'app/controllers/users/invitations_controller.rb.erb', "app/controllers/#{@user_plural}/invitations_controller.rb"
remote_template 'app/views/users/invitations/new.html.erb', "app/views/#{@user_plural}/invitations/new.html.erb"
remote_template 'app/views/users/invitations/_form.html.erb', "app/views/#{@user_plural}/invitations/_form.html.erb"

inject_into_file @model_file,
                 "\n\n  scope :invited, -> { where.not(invitation_created_at: nil).where(invitation_accepted_at: nil) }\n",
                after: "  validates :name, presence: true\n"

if @devise_roles&.any?
  inject_into_file "app/views/#{@user_plural}/invitations/_form.html.erb",
                    @role_form_text,
                    after: '      <%= render(Initforthe::FormErrorsComponent.new(f.object)) %>'
end

inject_into_file "app/views/#{@user_plural}/_#{@user_singular}.html.erb",
                     "\n        <% if #{@user_singular}.invited_to_sign_up? && !#{@user_singular}.invitation_accepted? %>\n          <span class=\"inline-flex flex-shrink-0 items-center rounded-full bg-orange-50 px-1.5 py-0.5 text-xs font-medium text-orange-600 ring-1 ring-inset ring-orange-600/20\">invited</span>\n        <% end %>",
                     after: "deleted</span>\n        <% end %>"
index_text = <<-ERB

    <% if policy(#{@authentication_model}).invite? %>
      <%= link_to new_#{@user_plural}_invitation_path, class: 'inline-flex ml-2 items-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:indigo-800 focus:outline-none focus:ring-2 focus:ring-primary-alt focus:ring-offset-2', data: { turbo_frame: 'modal' } do %>
        <%= heroicon 'envelope', variant: :solid, options: { class: 'w-5 h-5 mr-2 -ml-1' } %>
        Invite #{@authentication_model.titleize}
      <% end %>
    <% end %>
    ERB
inject_into_file "app/views/#{@user_plural}/index.html.erb",
                  index_text,
                  after: "Add User\n      <% end %>\n    <% end %>"

scope_text = <<-ERB
      <%= active_link_to #{@user_plural}_path(scope: 'invited'), active: { scope: 'invited' }, class: 'group relative min-w-0 flex-1 overflow-hidden bg-white py-4 px-4 text-center text-sm font-medium hover:bg-gray-50 focus:z-10', class_active: 'text-gray-900', class_inactive: 'text-gray-500 hover:text-gray-700' do %>
        <span>Invited</span>
        <span class="inline-flex items-center px-2 py-1 ml-2 text-xs font-medium text-gray-600 rounded-md bg-gray-50 ring-1 ring-inset ring-gray-500/10"><%= #{@authentication_model}.invited.count %></span>
        <% if is_active_link?(#{@user_plural}_path(scope: 'invited'), { scope: 'invited' }) %>
          <span aria-hidden="true" class="bg-indigo-600 absolute inset-x-0 bottom-0 h-0.5"></span>
        <% end %>
      <% end %>
ERB
inject_into_file "app/views/#{@user_plural}/_scopes.html.erb",
                  scope_text,
                  before: "    <%= active_link_to users_path(scope: :only_deleted)"
