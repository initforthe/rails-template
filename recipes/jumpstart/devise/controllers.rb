#  Devise
inject_into_file 'app/controllers/application_controller.rb', after: /layout \'simple\'\n/ do
<<-'RUBY'

  private

  def after_sign_in_path_for(_resource)
    dashboards_path
  end
RUBY
end
