# Adjust navigation for user
gsub_file 'app/views/application/_navbar.html.erb', '<%= vc_navbar user: nil do |c| %>', "<%= vc_navbar user: current_#{@user_singular} do |c| %>"
gsub_file 'app/views/application/_navbar.html.erb', '<!-- secondary links -->', "<%= link_to 'Sign out', destroy_#{@user_singular}_session_path, data: { turbo_method: :delete }, class: 'block px-4 py-2 text-sm text-indigo-600', role: 'menuitem', tabindex: '-1' %>"
gsub_file 'app/views/application/_navbar.html.erb', "<%= heroicon 'bars-3', variant: :outline, options: { class: 'w-5 h-5' } %>", "<%= current_#{@user_singular}.decorate.avatar(class: 'h-8 w-8 rounded-full') %>"
inject_into_file 'app/views/application/_navbar.html.erb',
                  "\n    <%= active_link_to #{@user_plural}_path, active: :exact, class: 'flex sm:inline-flex items-center sm:group px-3 py-2 text-base sm:text-sm font-medium rounded-md', class_active: 'bg-indigo-800 text-white', class_inactive: 'text-indigo-300 hover:bg-indigo-600 hover:text-white' do %>\n      <%= heroicon 'user', variant: :outline, options: { class: 'w-5 h-5 mr-2' } %>\n      <span class=\"flex-1\">#{@user_plural.capitalize}</span>\n    <% end %>",
                  after: "<span class=\"flex-1\">Dashboard</span>\n    <% end %>"
inject_into_file 'app/views/application/_navbar.html.erb', after: '<!-- Active: "bg-indigo-100", Not Active: "" -->' do
  "\n        <%= link_to edit_profile_account_path, data: { turbo_frame: 'modal' }, class: 'block px-4 py-2 text-sm text-indigo-600' do %>
        Your Details
      <% end %>\n"
end
