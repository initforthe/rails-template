# frozen_string_literal: true

remote_template 'babel.config.json', 'babel.config.json'
remote_template 'eslint.config.mjs', 'eslint.config.mjs'
remote_template 'prettierrc.json', '.prettierrc.json'
remote_template 'rubocop.yml', '.rubocop.yml'

system 'yarn add -D @babel/core @babel/eslint-parser globals @eslint/js @eslint/eslintrc @babel/preset-env eslint eslint-config-prettier eslint-plugin-prettier prettier sass-lint sass-lint-auto-fix'
