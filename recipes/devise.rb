# frozen_string_literal: true

generate 'devise:install'
generate :devise, @authentication_model

@user_singular = @authentication_model.underscore.downcase
@user_plural = @authentication_model.underscore.downcase.pluralize
@model_file = "app/models/#{@user_singular}.rb"
@devise_migration = Dir.glob("db/migrate/*devise_create_#{@user_singular.pluralize}.rb").first

inject_into_file @model_file,
                 "# #{@authentication_model.classify} class\n",
                 before: "class #{@authentication_model.classify} < ApplicationRecord"

recipe 'devise/factories'
recipe 'devise/name'
recipe 'devise/lockable'
recipe 'devise/settings'
recipe 'devise/gravatar'
recipe 'devise/views'
recipe 'devise/translations'
recipe 'devise/invitable' if @devise_invitable
recipe 'devise/two_factor' if @devise_2fa
recipe 'devise/masquerade' if @devise_masquerade
