# frozen_string_literal: true

# This recipe provides some optional ground work to get a project rolling such as the layout, and other functions.
#
# 1. Copy in base layout + devise where devise is present
# 2. Ensure a functional nav bar and sign out options
# 3. Provide a simple landing page / dashboard controller

recipe 'jumpstart/layouts'
recipe 'jumpstart/controllers'
recipe 'jumpstart/policies'
recipe 'jumpstart/devise' if @devise
recipe 'jumpstart/dashboard'
recipe 'jumpstart/decorators'
