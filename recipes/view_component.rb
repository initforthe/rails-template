add_file 'app/components/application_component.rb' do
  <<~RB
    # Application Component
    class ApplicationComponent < ViewComponent::Base; end
  RB
end
git submodule: 'add https://gitlab.com/initforthe/view-components.git vendor/extensions/initforthe_view_components'

gem 'initforthe_view_components', path: 'vendor/extensions/initforthe_view_components'
