# frozen_string_literal: true

remove_file 'db/seeds.rb'
remote_template 'db/seeds.rb.erb', 'db/seeds.rb'
