# frozen_string_literal: true

binstubs = %w[puma rubocop rspec-core sidekiq]

run "bundle binstubs #{binstubs.join(' ')}"
