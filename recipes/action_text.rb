# This invokes rspec, so should be before the `tests` recipe to avoid overwriting our changes.
rails_command 'action_text:install'

remove_file 'test/fixtures/action_text/rich_texts.yml'
