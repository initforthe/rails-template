remote_template 'config/locales/titles.en.yml', 'config/locales/titles.en.yml'
unless @devise || @jump_start
  append_to_file 'config/locales/titles.en.yml' do
    "  title: Title\n"
  end
end
