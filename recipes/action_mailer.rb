# frozen_string_literal: true

inject_into_file 'app/mailers/application_mailer.rb',
                 "# Application Mailer\n",
                 before: 'class ApplicationMailer < ActionMailer::Base'

gsub_file 'app/mailers/application_mailer.rb', /^.*default from.*/, '  default from: Settings.mail.sender'

@post_build_todo_items << 'Configure default mail sender in config/settings.yml'
